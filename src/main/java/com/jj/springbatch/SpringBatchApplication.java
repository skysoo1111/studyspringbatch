package com.jj.springbatch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
* spring boot 코드 레이아웃
 * 클래스가 패키지 정의를 포함하고 있지 않을 때, 디폴트 패키지로 정의된다.
 * 일반적으로 디폴트 패키지의 사용은 피해야한다.
 * 모든 jar의 모든 클래스를 읽어야 하므로 특정한 문제가 발생할 수 있다.
* @author skysoo
* @version 1.0.0
* @since 2019-10-10 23:05
**/
@EnableBatchProcessing // 배치기능 활성화
@SpringBootApplication //(exclude = { DataSourceAutoConfiguration.class }) //
public class SpringBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBatchApplication.class, args);
    }

}
