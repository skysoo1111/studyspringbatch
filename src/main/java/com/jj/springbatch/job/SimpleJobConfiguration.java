package com.jj.springbatch.job;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.stream.DoubleStream;


/**
 * Spring Batch에서 Job은 하나의 배치 작업 단위를 의미하는데, Job 안에는 여러 Step이 존재하고,
 * 각 Step 안에 Tasklet 혹은 Reader & Processor & Writer 묶음이 존재한다.
 * @author skysoo
 * @version 1.0.0
 * @since 2019-10-08 22:10
 **/

@Slf4j
@RequiredArgsConstructor  // 생성자 DI를 위한 lombok 어노테이션
@Configuration // Spring Batch의 모든 Job은 @Configuration으로 등록해서 사용한다.
public class SimpleJobConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job simpleJob(){
        return jobBuilderFactory.get("simpleJob") //simpleJob이란 이름의 Batch Job을 생성한다.
                .start(simpleStep1(null))
                .next(simpleStep2(null))
                .build();
    }


    /*Step에서는 실제로 Batch로 처리하고자 하는 기능과 설정을 모두 포함하는 로직
    * Job 내부의 Step들 간에 흐름을 제어할 필요*/
    @Bean
    @JobScope // Job Parameter를 이용하기 위한 어노테이션?
    public Step simpleStep1(@Value("#{jobParameters[requestData]}") String requestData){
//    public Step simpleStep1(){
        return stepBuilderFactory.get("simpleStep1")  //simpleStep1 이란 이름의 Batch Step을 생성한다.
                .tasklet((contribution, chunkContext) -> { // Step 안에서 수행될 기능들을 명시한다.
//                    throw new IllegalArgumentException("step1에서 실패합니다.");
                    log.info(">>>>> This is Step1");       // tasklet은 Step안에서 단일로 수행될 커스텀한 기능들을 선언할 때 사용한다.
                    log.info(">>>>> requestData = {}",requestData);
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    @JobScope // Job Parameter를 이용하기 위한 어노테이션?
    public Step simpleStep2(@Value("#{jobParameters[requestData]}") String requestData){
//    public Step simpleStep1(){
        return stepBuilderFactory.get("simpleStep2")  //simpleStep1 이란 이름의 Batch Step을 생성한다.
                .tasklet((contribution, chunkContext) -> { // Step 안에서 수행될 기능들을 명시한다.
                    log.info(">>>>> This is Step2");       // tasklet은 Step안에서 단일로 수행될 커스텀한 기능들을 선언할 때 사용한다.
                    log.info(">>>>> requestData = {}",requestData);
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

}
