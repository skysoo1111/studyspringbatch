package com.jj.springbatch.job;

import com.sun.jmx.snmp.tasks.Task;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Job Scope는 Step 선언문에서, Step Scope는 Tasklet 이나 ItemReader, ItemWriter, ItemProcessor 에서 사용 가능
 * Job Parameter의 타입 : Double, Long, Date, String (LocalDate, LocalDateTime은 String으로 받아서 타입변환 후 사용)
 * Job Parameter의 할당은 어플리케이션 실행시에 하지 않는다.
 * Bean의 생성 시점은 Scope의 실행 시점이다.
 *
 * @author skysoo
 * @version 1.0.0
 * @since 2019-10-11 오전 11:49
 **/
@Slf4j
@Configuration
@RequiredArgsConstructor
public class SimpleScopeConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job scopeJob(){
        return jobBuilderFactory.get("scopeJob")
                .start(scopeStep1(null))
                .next(scopeStep2())
                .next(simpleStep1())
                .build();
    }

    @Bean
    @JobScope
    public Step scopeStep1(@Value("#{jobParameters[requestData]}") String requestData){
        return stepBuilderFactory.get("scopeStep1")
                .tasklet((contribution, chunkContext) -> {
                    log.info(">>>>> This is scopeStep1");
                    log.info(">>>>> requestData = {}", requestData);
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    public Step scopeStep2(){
        return stepBuilderFactory.get("scopeStep2")
                .tasklet(scopeStep2Tasklet(null))
                .build();
    }

    @Bean
    @StepScope
    public Tasklet scopeStep2Tasklet(@Value("#{jobParameters[requestData]}") String requestData){
        return (contribution, chunkContext) -> {
            log.info(">>>>> This is scopeStep2Tasklet");
            log.info("RequestData {}", requestData);
            return RepeatStatus.FINISHED;
        };
    }

    /**
     * Bean을 메소드나 클래스 어느것을  통해서 생성해도 무방하나 Bean의 Scope는 Step이나 Job 이어야 한다.
     * 즉, Job Parameters를 사용하기 위해서는 @StepScope, @JobScope로 Bean을 생성해야 한다.
     * */
    private final SimpleJobTasklet tasklet1;

    public Step simpleStep1(){
        log.info(">>>>> definition simpleStep1");
        return stepBuilderFactory.get("simpleStep1")
                .tasklet(tasklet1)
                .build();
    }

    @Component
    @StepScope
    public class SimpleJobTasklet implements Tasklet{
        @Value("#{jobParameters[requestData]}")
        private String requestData;

        public SimpleJobTasklet(){
            log.info(">>>>> Tasklet 생성");
        }

        @Override
        public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
            log.info(">>>>> This is simpleStep1");
            log.info("requestData = {}", requestData);
            return RepeatStatus.FINISHED;
        }
    }

}
