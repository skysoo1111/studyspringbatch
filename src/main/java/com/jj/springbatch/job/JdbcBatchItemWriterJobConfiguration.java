package com.jj.springbatch.job;

import com.jj.springbatch.model.Pay;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import javax.sql.DataSource;

/**
 * @author skysoo
 * @version 1.0.0
 * @since 2019-10-11 오후 5:52
 **/

@Slf4j
@Configuration
@RequiredArgsConstructor
public class JdbcBatchItemWriterJobConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final DataSource dataSource;    // DataSource DI

    private static final int chunkSize = 10;

    @Bean
    public Job jdbcBatchItemWriterJob(){
        return jobBuilderFactory.get("jdbcBatchItemWriterJob")
                .start(jdbcBatchItemWriterStep())
                .build();
    }

    @Bean
    public Step jdbcBatchItemWriterStep(){
        return stepBuilderFactory.get("jdbcBatchItemWriterStep")
                .<Pay,Pay>chunk(chunkSize)  // <Pay,Pay> 첫번째 Pay는 Reader에서 반환할 타입, 두번째는 Writer에 Parameter로 넘어올 타입, chunkSize는 reader&writer가 묶일 범위
                .reader(jdbcBatchItemWriterReader())
                .writer(jdbcCursorItemWriter())
                .build();
    }

    /*
     * jdbcTemplate과 인터페이스 동일
     * 그러나 ItemReader()는 데이터를 streaming 할 수 있다는 장점 존재
     * */
    @Bean
    public JdbcCursorItemReader<Pay> jdbcBatchItemWriterReader() {
        return new JdbcCursorItemReaderBuilder<Pay>()
                .fetchSize(chunkSize)   // Database에서 한번에 가져올 양, Fetch Size만큼 가져와 read()를 통해 하나씩 가져옴
                .dataSource(dataSource) // DB 접근 객체
                .rowMapper(new BeanPropertyRowMapper<>(Pay.class))  // 쿼리 결과를 java 인스턴스로 매핑하기 위한 Mapper
                .sql("SELECT id,amount,tx_name,tx_date_time FROM pay")  // Reader로 사용할 쿼리
                .name("jdbcBatchItemWriterReader")   // Reader의 이름을 정함. Bean의 이름이 아닌 Spring Batch의 ExecutionContext에서 저장되어질 이름
                .build();
    }

    /**
     * reader에서 넘어온 데이터를 하나씩 출력하는 writer
     */
    @Bean // beanMapped()을 사용할때는 필수
    public JdbcBatchItemWriter<Pay> jdbcCursorItemWriter() {
        return new JdbcBatchItemWriterBuilder<Pay>()
                .dataSource(dataSource)
                .sql("insert into pay2(amount, tx_name, tx_date_time) values (:amount, :txName, :txDateTime)")
                .beanMapped()
                .build();
        /**
         * columnMapped 사용시
         * beanMapped와 columnMapped의 차이점은 넘겨받는 값이Pay.class 같은 Pojo 타입이냐, Map<String,Object> 같은 key-value 형태냐의 차이*/
//        new JdbcBatchItemWriterBuilder<Map<String, Object>>() // Map 사용
//                .columnMapped()
//                .dataSource(this.dataSource)
//                .sql("insert into pay2(amount, tx_name, tx_date_time) values (:amount, :txName, :txDateTime)")
//                .build();
    }


}
