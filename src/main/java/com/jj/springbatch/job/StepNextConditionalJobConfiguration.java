package com.jj.springbatch.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author skysoo
 * @version 1.0.0
 * @since 2019-10-11 오전 10:39
 **/

@Slf4j
@Configuration
@RequiredArgsConstructor
public class StepNextConditionalJobConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    /**
     * step1 실패 시나리오 : step1 > step3
     * step1 성공 시나리오 : step1 > step2 > step3
     * */
    @Bean
    public Job stepNextConditionalJob(){
        return jobBuilderFactory.get("stepNextConditionalJob")
                .start(conditionalStep1())
                    .on("FAILED")   //FAILED일 경우
                    .to(conditionalStep3()) //step3으로 이동
                    .on("*")    //결과에 상관 없이
                    .end()  //step3으로 이동하면 종료
                .from(conditionalStep1())   //step1로부터
                    .on("*")    //FAILED 이외의 모든 경우
                    .to(conditionalStep2()) //step2로 이동한다.
                    .next(conditionalStep3()) // step2가 종료되면 step3으로 이동한다.
                    .on("*")    //step3의 결과 관계 없이
                    .end()  //step3으로 이동하면 flow가 종료된다.
                .end()  // job 종료
                .build();
    }
    /**
     * on() : 캐치할 ExitStatus 지정
     * to() : 다음으로 이동할 step 지정
     * from() : 일종의 이벤트 리스너 역할, 상태값을 보고 일치하는 상태라면 to()에 포함된 다음 step을 호출
     * end() : Flow Builder를 반환하는 end와 Flow Builder를 종료하는 end 2개가 있음*/

    @Bean
    public Step conditionalStep1(){
        return stepBuilderFactory.get("conditionalStep1")
                .tasklet((contribution, chunkContext) -> {
                    log.info(">>>>> This is conditionalStep1");
                    /**
                        ExitStatus를 보고 FAILED로 지정한다.
                        해당 status를 보고 flow가 진행된다.
                     **/
//                    contribution.setExitStatus(ExitStatus.FAILED);
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    public Step conditionalStep2(){
        return stepBuilderFactory.get("conditionalStep2")
                .tasklet((contribution, chunkContext) -> {
                    log.info(">>>>> This is conditionalStep2");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    public Step conditionalStep3(){
        return stepBuilderFactory.get("conditionalStep3")
                .tasklet((contribution, chunkContext) -> {
                    log.info(">>>>> This is conditionalStep3");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

}
