package com.jj.springbatch.job;

import com.jj.springbatch.model.Pay;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import javax.sql.DataSource;

/**
 * Paging : 페이지라는 chunk로 db에서 데이터를 한번에 조회해오는 방식
 * Cursor : JDBC ResultSet의 기본 기능으로 db와 커넥션을 맺은 후 한칸씩 지속적으로 가져오는 방식
 *
 * CursorItemReader 사용시 주의 사항
 * Cursor는 하나의 connection으로  Batch가 끝날때까지 사용되기 때문에 Batch가 끝나기 전에 DB와의 커넥션이 먼저 끊길 수 있다.
 * 즉, Batch 수행 시간이 오래 걸린다면 PagingItemReader를 사용해라.
 * @author skysoo
 * @version 1.0.0
 * @since 2019-10-11 오후 2:38
 **/
@Slf4j
@Configuration
@RequiredArgsConstructor
public class JdbcCursorItemReaderJobConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final DataSource dataSource;    // DataSource DI

    private static final int chunkSize = 10;

    @Bean
    public Job jdbcCursorItemReaderJob(){
        return jobBuilderFactory.get("jdbcCursorItemReaderJob")
                .start(jdbcCursorItemReaderStep())
                .build();
    }

    @Bean
    public Step jdbcCursorItemReaderStep(){
        return stepBuilderFactory.get("jdbcCursorItemReaderStep")
                .<Pay,Pay>chunk(chunkSize)  // <Pay,Pay> 첫번째 Pay는 Reader에서 반환할 타입, 두번째는 Writer에 Parameter로 넘어올 타입, chunkSize는 reader&writer가 묶일 범위
                .reader(jdbcCursorItemReader())
                .writer(jdbcCursorItemWriter())
                .build();
    }

    /*
    * jdbcTemplate과 인터페이스 동일
    * 그러나 ItemReader()는 데이터를 streaming 할 수 있다는 장점 존재
    * */
    @Bean
    public JdbcCursorItemReader<Pay> jdbcCursorItemReader() {
        return new JdbcCursorItemReaderBuilder<Pay>()
                .fetchSize(chunkSize)   // Database에서 한번에 가져올 양, Fetch Size만큼 가져와 read()를 통해 하나씩 가져옴
                .dataSource(dataSource) // DB 접근 객체
                .rowMapper(new BeanPropertyRowMapper<>(Pay.class))  // 쿼리 결과를 java 인스턴스로 매핑하기 위한 Mapper
                .sql("SELECT id,amount,tx_name,tx_date_time FROM pay")  // Reader로 사용할 쿼리
                .name("jdbcCursorItemReader")   // Reader의 이름을 정함. Bean의 이름이 아닌 Spring Batch의 ExecutionContext에서 저장되어질 이름
                .build();
    }

    private ItemWriter<Pay> jdbcCursorItemWriter() {
        return list->{
            for (Pay pay : list){
                log.info("Current Pay={}",pay);
            }
        };
    }

}
